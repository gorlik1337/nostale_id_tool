#ifndef SETTINGSVIEW_H
#define SETTINGSVIEW_H

#include <QWidget>
#include "redesignable.h"
#include "retranslatable.h"
#include "ui_settingsview.h"

class MainController;
class SettingsController;

class SettingsView : public QWidget, public Retranslatable, public Redesignable {
    Q_OBJECT

public:
    SettingsView(MainController* mainController);

    ~SettingsView();

    void initialize();

    void resizeEvent(QResizeEvent* resizeEvent);

    void retranslate() override;

    void redesign() override;

private:
    Ui::SettingsView ui_;

    SettingsController* controller_;

private slots:
    void languageComboBoxCurrentIndexChanged(int currentIndex);

    void continuePushButtonClicked();

    void lightRadioButtonClicked();

    void darkRadioButtonClicked();

    void exploreDataFilePushButtonClicked();

    void exploreLangFilePushButtonClicked();

    void savePushButtonClicked();
};

#endif // !SETTINGSVIEW_H
