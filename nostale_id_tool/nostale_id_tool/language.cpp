#include "language.h"

Language::Language(const QMap<QString, QString>& data) :
    data_(data)
{}

const QString Language::getValue(const QString& key) const
{
    return data_.value(key);
}
