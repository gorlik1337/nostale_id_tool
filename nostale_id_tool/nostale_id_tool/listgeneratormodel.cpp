#include "listgeneratormodel.h"
#include "listgeneratorcontroller.h"
#include "typeparser.h"

ListGeneratorModel::ListGeneratorModel(ListGeneratorController* controller) :
    controller_(controller),
    currentType_(nullptr),
    selectedFileType_(nullptr)
{}

ListGeneratorModel::~ListGeneratorModel()
{}

void ListGeneratorModel::initialize()
{
    types_ = TypeParser("data/types.json").parse();
}

const QStringList ListGeneratorModel::getTypeNames() const
{
    QStringList result;
    for (auto type : types_) {
        result.append(type.name);
    }
    return result;
}

Type* ListGeneratorModel::getCurrentType()
{
    return currentType_;
}

void ListGeneratorModel::setCurrentType(int index)
{
    currentType_ = &types_[index];
}

const QStringList ListGeneratorModel::getWhitelist() const
{
    QStringList result;
    for (auto whitelistEntry : currentType_->whitelist) {
        result.append(whitelistEntry.name);
    }
    return result;
}

const QStringList ListGeneratorModel::getElements() const
{
    QStringList result;
    for (auto element : currentType_->elements) {
        result.append(element.name);
    }
    return result;
}

void ListGeneratorModel::appendToSelectedElements(ElementEntry* element)
{
    selectedElements_.append(element);
}

void ListGeneratorModel::clearSelectedElements()
{
    selectedElements_.clear();
}

FileType* ListGeneratorModel::getSelectedFileType()
{
    return selectedFileType_;
}

void ListGeneratorModel::setSelectedFileType(FileType* fileType)
{
    selectedFileType_ = fileType;
}
