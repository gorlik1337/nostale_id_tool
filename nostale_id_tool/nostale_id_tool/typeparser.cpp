#include "typeparser.h"
#include "whitelistentry.h"
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonObject>

TypeParser::TypeParser(const QString& filepath) :
    filepath_(filepath)
{}

TypeParser::~TypeParser()
{}

QVector<Type> TypeParser::parse()
{
    QVector<Type> result;
    QFile file(filepath_);
    if (!file.open(QIODevice::ReadOnly)) {
        // TODO: error
        return result;
    }

    auto jsonDocument = QJsonDocument::fromJson(file.readAll());
    file.close();

    auto jsonObject = jsonDocument.object().value("types").toObject();
    const auto types = jsonObject.keys();

    /* Append each type to the vector */
    for (auto type : types) {
        result.append(parseType(type, jsonObject));
    }
    return result;
}

Type TypeParser::parseType(const QString& type, const QJsonObject& object)
{
    Type result;
    result.name = type;
    const auto tempObject = object.value(type).toObject();
    result.dataFileName = tempObject.value("data").toString();
    result.langFileName = tempObject.value("lang").toString();
    result.isWhitelistActive = false;

    /* Add all whitelist elements */
    QVector<QString> tempList;
    auto jsonArray = tempObject.value("whitelist").toArray();
    parseArrayToList<WhitelistEntry, QString>(jsonArray, result.whitelist);

    /* Add all available elements */
    tempList.clear();
    jsonArray = tempObject.value("elements").toArray();
    //parseArrayToList<ElementEntry, QString>(jsonArray, result.elements);

    for (auto jsonValue : jsonArray) {
        if (jsonValue.isString()) {
            const auto name = jsonValue.toString();
            const auto simplified = name.simplified();

            bool skipValue = false;
            for (auto c : name) {
                if ((c < '!' || c == 0x7F) && c != '\t' && c != '\r' && c != '\n') {
                    skipValue = true;
                    break;
                }
            }
            if (skipValue) {
                continue;
            }

            if (!tempList.contains(name)) {
                result.elements.append(ElementEntry(name));
                tempList.append(name);
            }
        }
    }
    return result;
}