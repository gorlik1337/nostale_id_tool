#include "settingsmodel.h"
#include "settingscontroller.h"
#include "languageparser.h"
#include "language.h"

SettingsModel::SettingsModel(SettingsController* controller) :
    controller_(controller),
    settings_(settings_->getInstance())
{
    // TODO: make languages rereadable
    settings_->setLanguages(LanguageParser("assets/lang/").parse());
}

SettingsModel::~SettingsModel()
{}

void SettingsModel::initialize()
{
    settings_->setCurrentLanguage(settings_->getDefaultLanguage());
    settings_->initialize();
}

Language* SettingsModel::getCurrentLanguage()
{
    return settings_->getCurrentLanguage();
}

const QStringList SettingsModel::getLanguages() const
{
    QStringList result;
    const auto& languages = settings_->getLanguages();
    for (auto& language : languages) {
        result.append(language->getValue("language").arg(language->getValue("abbreviation")));
    }
    return result;
}

const int SettingsModel::getCurrentLanguageIndex()
{
    auto languages = settings_->getLanguages();
    for (short i = 0; i < languages.size(); ++i) {
        if (settings_->getCurrentLanguage() == languages[i]) {
            return i;
        }
    }
    return -1;
}

void SettingsModel::setCurrentLanguageIndex(int index)
{
    auto languages = settings_->getLanguages();
    if (index >= 0 && index < languages.size()) {
        settings_->setCurrentLanguage(languages[index]);
        settings_->save();
        return;
    }
}

Design SettingsModel::getDesign()
{
    return settings_->getDesign();
}
