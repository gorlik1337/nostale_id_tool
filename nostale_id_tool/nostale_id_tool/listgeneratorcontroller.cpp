#include "listgeneratorcontroller.h"
#include "listgeneratorview.h"
#include "listgeneratormodel.h"
#include "language.h"
#include "settings.h"
#include "filetype.h"
#include "elemententry.h"
#include "workerthread.h"
#include <QDir>
#include <QDesktopServices>
#include <QFileInfo>
#include <QDateTime>
#include <QResizeEvent>
#include <QFileDialog>

ListGeneratorController::ListGeneratorController(ListGeneratorView* view, Ui::ListGeneratorView* ui, WorkerThread* workerThread) :
	view_(view),
	ui_(ui),
	model_(new ListGeneratorModel(this)),
	workerThread_(workerThread)
{}

ListGeneratorController::~ListGeneratorController()
{
	delete model_;
}

void ListGeneratorController::initialize()
{
	ui_->txtRadioButton->setChecked(true);
	ui_->outputFormatTextEdit->setReadOnly(true);

	model_->initialize();

	setSelectedFileType(&FileType::txt);

	loadTypesToListWidget();
	loadWhitelistToListWidget();
	loadElementsToListWidget();

	setPredefinedOutputPath();
}

void ListGeneratorController::resizeEvent(QResizeEvent* resizeEvent)
{
	const auto width = view_->width();
	const auto height = view_->height();

#pragma region groupBoxOutputFormat
	ui_->outputFormatGroupBox->resize(width - 19, ui_->outputFormatGroupBox->height());
	ui_->outputFormatTextEdit->resize(width - 39, ui_->outputFormatTextEdit->height());
#pragma endregion

#pragma region groupBoxOutputPath
	ui_->outputPathGroupBox->resize(width - 219, ui_->outputFormatGroupBox->height());
	ui_->outputPathLineEdit->resize(width - 239, ui_->outputPathLineEdit->height());
	ui_->explorePushButton->move(width - 350, ui_->explorePushButton->y());
	ui_->showInExplorerPushButton->move(width - 350, ui_->showInExplorerPushButton->y());
#pragma endregion

#pragma region groupBoxFilter
	ui_->whitelistGroupBox->resize(ui_->whitelistGroupBox->width(), height - 288);
	ui_->whitelistListWidget->resize(ui_->whitelistListWidget->width(), height - 338);
	ui_->whitelistActiveCheckBox->move(ui_->whitelistActiveCheckBox->x(), ui_->whitelistGroupBox->height() - 31);
#pragma endregion

#pragma region groupBoxType
	ui_->typeGroupBox->resize(ui_->elementsListWidget->x() - ui_->typeGroupBox->x() - 9, height - ui_->typeGroupBox->y() - 8);
	ui_->typeListWidget->resize(width - 619, height - 338);
#pragma endregion

#pragma region Available elements
	ui_->elementsAvailableLabel->move(width - 200, ui_->elementsAvailableLabel->y());
	ui_->elementsListWidget->move(width - 200, ui_->elementsListWidget->y());
	ui_->addAllPushButton->move(width - 200, height - 69);
	ui_->clearPushButton->move(width - 83, height - 69);
	ui_->elementsListWidget->resize(ui_->elementsListWidget->width(), ui_->addAllPushButton->y() - ui_->elementsListWidget->y() - 4);
	ui_->extractPushButton->move(width - 99, height - 29);
#pragma endregion
}

void ListGeneratorController::retranslate()
{
	auto language = Settings::getInstance()->getCurrentLanguage();
	const QString prefix = "listGenerator.";

#pragma region groupBoxOutputFormat
	auto tempPrefix = prefix + "groupBoxOutputFormat.";
	ui_->outputFormatGroupBox->setTitle(language->getValue(tempPrefix + "title"));
	ui_->outputFormatLabel->setText(language->getValue(tempPrefix + "label") + " {id}{t}{translation}");
#pragma endregion

#pragma region groupBoxOutputPath
	tempPrefix = prefix + "groupBoxOutputPath.";
	ui_->outputPathGroupBox->setTitle(language->getValue(tempPrefix + "title"));
	ui_->outputPathLabel->setText(language->getValue(tempPrefix + "label"));
	ui_->txtRadioButton->setText(language->getValue(tempPrefix + "txtFile") + " (.txt)");
	ui_->csvRadioButton->setText(language->getValue(tempPrefix + "csvFile") + " (.csv)");
	ui_->htmlRadioButton->setText(language->getValue(tempPrefix + "htmlFile") + " (.html)");
	ui_->explorePushButton->setText(language->getValue(tempPrefix + "exploreButton"));
	ui_->showInExplorerPushButton->setText(language->getValue(tempPrefix + "showInExplorerButton"));
#pragma endregion

#pragma region groupBoxFilter
	tempPrefix = prefix + "groupBoxWhitelist.";
	ui_->whitelistGroupBox->setTitle(language->getValue(tempPrefix + "title"));
	ui_->whitelistLabel->setText(language->getValue(tempPrefix + "label"));
	ui_->checkWhitelistEntriesPushButton->setText(language->getValue(tempPrefix + "checkButton"));
	ui_->uncheckWhitelistEntriesPushButton->setText(language->getValue(tempPrefix + "uncheckButton"));
	ui_->whitelistActiveCheckBox->setText(language->getValue(tempPrefix + "whitelistActive"));
#pragma endregion

#pragma region groupBoxType
	tempPrefix = prefix + "groupBoxType.";
	ui_->typeGroupBox->setTitle(language->getValue(tempPrefix + "title"));
	ui_->typeLabel->setText(language->getValue(tempPrefix + "label"));
#pragma endregion

#pragma region Available elements
	ui_->elementsAvailableLabel->setText(language->getValue(prefix + "labelElementsAvailable"));
	ui_->addAllPushButton->setText(language->getValue(prefix + "buttonAddAll"));
	ui_->clearPushButton->setText(language->getValue(prefix + "buttonClear"));
	ui_->extractPushButton->setText(language->getValue(prefix + "buttonExtract"));
#pragma endregion
}

void ListGeneratorController::redesign()
{
	// TODO: redesign UI
}

void ListGeneratorController::loadTypesToListWidget()
{
	const auto typeNames = model_->getTypeNames();
	ui_->typeListWidget->addItems(typeNames);
	if (typeNames.size() != 0) {
		ui_->typeListWidget->setCurrentRow(0);
		model_->setCurrentType(0);
	}
}

void ListGeneratorController::loadWhitelistToListWidget()
{
	ui_->whitelistListWidget->clear();
	const auto list = model_->getCurrentType()->whitelist;

	for (auto whitelistEntry : list) {
		ui_->whitelistListWidget->addItem(whitelistEntry.name);
		auto item = ui_->whitelistListWidget->item(ui_->whitelistListWidget->count() - 1);
		item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
		item->setCheckState(whitelistEntry.isChecked ? Qt::Checked : Qt::Unchecked);
	}
}

void ListGeneratorController::loadElementsToListWidget()
{
	ui_->elementsListWidget->clear();
	auto list = model_->getCurrentType()->elements;

	for (int i = 0; i < list.size(); ++i) {
		/* Convert whitespaces to readable format. Remove illegal characters */
		auto name = QString(list[i].name).replace('\t', "\\t");
		name = name.replace('\r', "\\r");
		name = name.replace('\n', "\\n");
		name = name.simplified();
		if (name.isEmpty()) {
			list.removeAt(i);
		}
		else {
			ui_->elementsListWidget->addItem(name);
		}
	}
}

int ListGeneratorController::getWhitelistListWidgetItemIndex(QListWidgetItem* item)
{
	const auto& whitelist = model_->getCurrentType()->whitelist;
	for (int i = 0; i < whitelist.size(); ++i) {
		if (whitelist[i].name == item->text()) {
			return i;
		}
	}
	return -1;
}

void ListGeneratorController::updateWhitelistEntryCheckState(int index, QListWidgetItem* item)
{
	if (item == nullptr) {
		item = ui_->whitelistListWidget->item(index);
	}
	const bool isChecked = item->checkState() == Qt::Checked;
	model_->getCurrentType()->whitelist[index].isChecked = isChecked;
}

void ListGeneratorController::updateIsWhitelistActive()
{
	model_->getCurrentType()->isWhitelistActive = ui_->whitelistActiveCheckBox->isChecked();
}

int ListGeneratorController::getElementItemIndex(QListWidgetItem* item)
{
	const auto& elements = model_->getCurrentType()->elements;
	for (int i = 0; i < elements.size(); ++i) {
		auto name = item->text();
		name = name.replace("\\t", "\t");
		name = name.replace("\\r", "\r");
		name = name.replace("\\n", "\n");
		if (elements[i].name == name) {
			return i;
		}
	}
	return -1;
}

ElementEntry* ListGeneratorController::getElement(QListWidgetItem* item)
{
	return &model_->getCurrentType()->elements[getElementItemIndex(item)];
}

void ListGeneratorController::appendElementToOutputFormat(ElementEntry* element)
{
	auto appendable = QString(element->name).replace('\t', "\\t");
	appendable = appendable.replace('\r', "\\r");
	appendable = appendable.replace('\n', "\\n");
	ui_->outputFormatTextEdit->setText(ui_->outputFormatTextEdit->toPlainText() + QString("{%1}").arg(appendable));
	model_->appendToSelectedElements(element);
}

void ListGeneratorController::appendAllElementsToOutputFormat()
{
	for (auto element : model_->getCurrentType()->elements) {
		appendElementToOutputFormat(&element);
	}
}

void ListGeneratorController::clearOutputFormat()
{
	ui_->outputFormatTextEdit->clear();
}

void ListGeneratorController::openOutputPathInExplorer()
{
	const auto filepath = ui_->outputPathLineEdit->text();
	const auto filename = QFileInfo(filepath).fileName();
	const auto lastIndex = filepath.lastIndexOf(filename);
	const auto dir = ui_->outputPathLineEdit->text().remove(lastIndex, filename.size());

	if (QDir(dir).exists()) {
		QDesktopServices::openUrl(dir);
	}
	else {
		// TODO: error
	}
}

void ListGeneratorController::setCurrentType(int index)
{
	ui_->outputFormatTextEdit->clear();
	model_->setCurrentType(index);
	ui_->whitelistActiveCheckBox->setChecked(model_->getCurrentType()->isWhitelistActive);
	loadWhitelistToListWidget();
	loadElementsToListWidget();
	setPredefinedOutputPath();
}

void ListGeneratorController::setWhitelistCheckState(Qt::CheckState checkState)
{
	auto whitelist = model_->getCurrentType()->whitelist;
	for (int i = 0; i < whitelist.size(); ++i) {
		auto item = ui_->whitelistListWidget->item(i);
		item->setCheckState(checkState);
		updateWhitelistEntryCheckState(i, item);
	}
}

void ListGeneratorController::setPredefinedOutputPath()
{
	// TODO: use file ending too
	QString text = QString("%1/%2_%3.%4").arg(QDir::currentPath()).arg(QDateTime::currentDateTime().toString("hh-mm-ss_dd-MM-yyyy"));
	ui_->outputPathLineEdit->setText(text.arg(model_->getCurrentType()->name).arg(model_->getSelectedFileType()->name));
}

void ListGeneratorController::setSelectedFileType(FileType* fileType)
{
	model_->setSelectedFileType(fileType);
}

void ListGeneratorController::promptForOutputPath()
{
	// TODO: Implement translation for caption
	auto fileName = QFileDialog::getSaveFileName(view_, "Save file", QDir::currentPath(), "All files (*.*)");
	if (!fileName.isEmpty()) {
		ui_->outputPathLineEdit->setText(fileName);
	}
}
