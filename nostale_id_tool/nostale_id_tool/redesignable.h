#ifndef REDESIGNABLE_H
#define REDESIGNABLE_H

class Redesignable {
public:
    virtual void redesign() = 0;
};

#endif // !REDESIGNABLE_H
