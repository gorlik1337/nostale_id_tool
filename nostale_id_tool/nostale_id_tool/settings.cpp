#include "settings.h"
#include "language.h"
#include "settingscomposer.h"
#include "settingsparser.h"
#include "settingswriter.h"

Settings Settings::instance_;

Settings::Settings() :
    currentLanguage_(nullptr),
    design_(Design::Light)
{}

Settings::~Settings()
{
    for (auto language : languages_) {
        delete language;
    }
}

void Settings::initialize()
{
    const QString filepath = "data/settings.json";
    QFile file(filepath);
    if (file.size() == 0) {
        file.remove();
    }

    isInitialStart_ = !file.exists();
    if (!isInitialStart_) {
        SettingsParser(filepath).parse();
    }
}

Language* Settings::getLanguage(const QString& abbreviation)
{
    for (auto& language : languages_) {
        if (language->getValue("abbreviation").toLower() == QString(abbreviation).toLower()) {
            return language;
        }
    }
    return nullptr;
}

const QVector<Language*>& Settings::getLanguages() const
{
    return languages_;
}

void Settings::setLanguages(const QVector<Language*>& value)
{
    languages_ = value;
}

const Design Settings::getDesign() const
{
    return design_;
}

void Settings::setDesign(Design value)
{
    design_ = value;
}

Language* Settings::getDefaultLanguage()
{
    for (auto& language : languages_) {
        /* Choose English as default language */
        if (language->getValue("abbreviation") == "en-US") {
            return language;
        }
    }
    return nullptr;
}

Language* Settings::getCurrentLanguage()
{
    return currentLanguage_;
}

void Settings::setCurrentLanguage(Language* value)
{
    currentLanguage_ = value;
}

const QString Settings::getDataFilePath() const
{
    return dataFilePath_;
}

void Settings::setDataFilePath(const QString& value)
{
    dataFilePath_ = value;
}

const QString Settings::getLangFilePath() const
{
    return langFilePath_;
}

void Settings::setLangFilePath(const QString& value)
{
    langFilePath_ = value;
}

void Settings::save()
{
    SettingsWriter writer("data/settings.json");
    writer.write(SettingsComposer::getJsonData());
}

const bool Settings::isInitialStart() const
{
    return isInitialStart_;
}

Settings* Settings::getInstance()
{
    return &instance_;
}
