#ifndef STARTCONTROLLER_H
#define STARTCONTROLLER_H

#include <QObject>
#include "redesignable.h"
#include "retranslatable.h"

class StartView;
namespace Ui {
    class StartView;
}
class StartModel;

class StartController : public QObject, public Retranslatable, public Redesignable {
    Q_OBJECT

public:
    StartController(StartView* view, Ui::StartView* ui);

    ~StartController();

    void retranslate() override;

    void redesign() override;

private:
    StartView* view_;

    Ui::StartView* ui_;

    StartModel* model_;

};

#endif // !STARTCONTROLLER_H
