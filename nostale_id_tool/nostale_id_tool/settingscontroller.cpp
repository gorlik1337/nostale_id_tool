#include "settingscontroller.h"
#include "settingsview.h"
#include "settingsmodel.h"
#include "language.h"
#include "maincontroller.h"
#include "settings.h"
#include <QFileDialog>
#include <QResizeEvent>
#include <QStandardPaths>
#include <QFileInfo>
#include <QFileInfoList>
#include <QDirIterator>
#include <QSettings>

SettingsController::SettingsController(SettingsView* view, Ui::SettingsView* ui, MainController* mainController) :
    view_(view),
    ui_(ui),
    settings_(Settings::getInstance()),
    mainController_(mainController),
    model_(new SettingsModel(this))
{}

SettingsController::~SettingsController()
{
    delete model_;
}

void SettingsController::initialize()
{
    /* Initialize languageComboBox */
    ui_->languageComboBox->addItems(model_->getLanguages());

    /* Initialize the model */
    model_->initialize();

    ui_->languageComboBox->setCurrentIndex(model_->getCurrentLanguageIndex());

    changeDesign(model_->getDesign());

    ui_->dataFileLineEdit->setText(settings_->getDataFilePath());
    ui_->langFileLineEdit->setText(settings_->getLangFilePath());


#ifdef Q_OS_WIN
    suggestProbableFilePaths(getProbableFilePaths());
    saveSettings();
#endif // Q_OS_WIN
}

void SettingsController::resizeEvent(QResizeEvent* resizeEvent)
{
    /* Resize the GroupBox */
    ui_->listGeneratorGroupBox->resize(view_->width() - 19, ui_->listGeneratorGroupBox->height());

    /* Move the explore PushButtons */
    const auto groupBoxWidth = ui_->listGeneratorGroupBox->width();
    const auto posX = groupBoxWidth - 51;
    ui_->exploreDataFilePushButton->move(posX, ui_->exploreDataFilePushButton->y());
    ui_->exploreLangFilePushButton->move(posX, ui_->exploreLangFilePushButton->y());

    /* Resize the LineEdits */
    const auto newWidth = groupBoxWidth - 160;
    ui_->dataFileLineEdit->resize(newWidth, ui_->dataFileLineEdit->height());
    ui_->langFileLineEdit->resize(newWidth, ui_->langFileLineEdit->height());

    /* Move the save PushButton */
    ui_->savePushButton->move(groupBoxWidth - 81, ui_->savePushButton->y());
}

void SettingsController::retranslate()
{
    auto language = model_->getCurrentLanguage();
    ui_->designContrastLabel->setText(language->getValue("settings.groupBoxUI.design.title"));
    ui_->lightRadioButton->setText(language->getValue("settings.groupBoxUI.design.light"));
    ui_->darkRadioButton->setText(language->getValue("settings.groupBoxUI.design.dark"));
    ui_->idToolGroupBox->setTitle(language->getValue("settings.groupBoxUI.title"));
    ui_->listGeneratorGroupBox->setTitle(language->getValue("settings.groupBoxListGenerator.title"));
    ui_->dataFileLabel->setText(language->getValue("settings.groupBoxListGenerator.dataFile"));
    ui_->langFileLabel->setText(language->getValue("settings.groupBoxListGenerator.langFile"));
    ui_->savePushButton->setText(language->getValue("settings.groupBoxListGenerator.saveButton"));
}

void SettingsController::changeDataFilePath()
{
    const auto openFileName = getOpenFileName();
    if (!openFileName.isNull() && !openFileName.isEmpty()) {
        Settings::getInstance()->setDataFilePath(openFileName);
        ui_->dataFileLineEdit->setText(openFileName);
    }
}

void SettingsController::changeLangFilePath()
{
    const auto openFileName = getOpenFileName();
    if (!openFileName.isNull() && !openFileName.isEmpty()) {
        Settings::getInstance()->setLangFilePath(openFileName);
        ui_->langFileLineEdit->setText(openFileName);
    }
}

void SettingsController::changeLanguage(int languageIndex)
{
    model_->setCurrentLanguageIndex(languageIndex);

    /* Only retranslate if there were languages found. */
    if (model_->getCurrentLanguage() != nullptr) {
        mainController_->retranslate();
    }
}

void SettingsController::changeDesign(Design design)
{
    Settings::getInstance()->setDesign(design);
    mainController_->redesign();
    if (design == Design::Light) {
        ui_->lightRadioButton->setChecked(true);
    }
    else {
        ui_->darkRadioButton->setChecked(true);
    }
}

void SettingsController::saveSettings()
{
    Settings::getInstance()->save();
}

void SettingsController::saveDataFilePaths()
{
    settings_->setDataFilePath(ui_->dataFileLineEdit->text());
    settings_->setLangFilePath(ui_->langFileLineEdit->text());
}

const QString SettingsController::getOpenFileName() const
{
#ifdef Q_OS_WIN
    // TODO: retranslate UI
    QString filepath = QFileDialog::getOpenFileName(view_, "Open image", "", "NosTale game files (*.nos)");
#elif defined(Q_OS_UNIX) || defined (Q_OS_LINUX) || defined (Q_OS_MACOS)
    QString filepath = QFileDialog::getOpenFileName(view_, "Open image", QStandardPaths::locate(QStandardPaths::HomeLocation, QString(), QStandardPaths::LocateDirectory));
#endif // Q_OS_UNIX || Q_OS_LINUX || Q_OS_MACOS
    return filepath;
}

#ifdef Q_OS_WIN
const QStringList SettingsController::getProbableFilePaths() const
{
    // TODO transfer these filenames to central point
    static const QString dataFilename = "NSgtdData.nos";
    static const QString langFilename = "NSlangData.nos";
    static bool dataFilenameFound = false;
    static bool langFilenameFound = false;

    QSettings settings("HKEY_LOCAL_MACHINE\\SOFTWARE\\WOW6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Nostale(DE)_is1\\", QSettings::NativeFormat);
    auto installLocation = settings.value("InstallLocation").toString();

    /* If registry key/value not found, leave silently */
    QStringList result;
    if (installLocation.isNull()) {
        return result;
    }

    /* Search for data and lang file recursively in Nostale directory */
    QDirIterator it(installLocation, QStringList() << dataFilename << langFilename, QDir::Files, QDirIterator::Subdirectories);
    while (it.hasNext()) {
        const auto path = QDir(it.next()).path();

        if (path.endsWith(dataFilename, Qt::CaseInsensitive)) {
            if (dataFilenameFound) {
                continue;
            }
            dataFilenameFound = true;
        }
        else if (path.endsWith(langFilename, Qt::CaseInsensitive)) {
            if (langFilenameFound) {
                continue;
            }
            langFilenameFound = true;
        }
        result.append(path);
        if (result.size() == 2) {
            break;
        }
    }
    return result;
}

void SettingsController::suggestProbableFilePaths(const QStringList& filepaths)
{
    Settings* settings = Settings::getInstance();

    for (auto filepath : filepaths) {
        if (settings->getDataFilePath().isEmpty() && filepath.endsWith("NSgtdData.NOS", Qt::CaseInsensitive)) {
            ui_->dataFileLineEdit->setText(filepath);
        }
        else if (settings->getLangFilePath().isEmpty() && filepath.endsWith("NSlangData.NOS", Qt::CaseInsensitive)) {
            ui_->langFileLineEdit->setText(filepath);
        }
    }
}
#endif // Q_OS_WIN
