#ifndef SETTINGSWRITER_H
#define SETTINGSWRITER_H

#include <QObject>
#include <QFile>
#include "filewriter.h"

class SettingsWriter : public QObject, public FileWriter {
    Q_OBJECT

public:
    SettingsWriter(const QString& filepath);

    ~SettingsWriter();

    void write(const QByteArray& textBytes) override;

private:
    QFile file_;

};

#endif // !SETTINGSWRITER_H
