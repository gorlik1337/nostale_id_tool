#ifndef LISTGENERATORCONTROLLER_H
#define LISTGENERATORCONTROLLER_H

#include <QObject>
#include "redesignable.h"
#include "retranslatable.h"

class FileType;
class ElementEntry;
class QListWidgetItem;
class ListGeneratorView;
class QResizeEvent;
namespace Ui {
    class ListGeneratorView;
}
class ListGeneratorModel;
class WorkerThread;

class ListGeneratorController : public QObject, public Retranslatable, public Redesignable {
    Q_OBJECT

public:
    ListGeneratorController(ListGeneratorView* view, Ui::ListGeneratorView* ui, WorkerThread* workerThread);

    ~ListGeneratorController();

    void initialize();

    void resizeEvent(QResizeEvent* resizeEvent);

    void retranslate() override;

    void redesign() override;

    void openOutputPathInExplorer();

    void setCurrentType(int index);

    void setWhitelistCheckState(Qt::CheckState checkState);

    int getWhitelistListWidgetItemIndex(QListWidgetItem* item);

    void updateWhitelistEntryCheckState(int index, QListWidgetItem* item = nullptr);

    void updateIsWhitelistActive();

    int getElementItemIndex(QListWidgetItem* item);

    ElementEntry* getElement(QListWidgetItem* item);

    void appendElementToOutputFormat(ElementEntry* element);

    void appendAllElementsToOutputFormat();

    void clearOutputFormat();

    void setPredefinedOutputPath();

    void setSelectedFileType(FileType* fileType);

    void promptForOutputPath();

private:
    ListGeneratorView* view_;

    Ui::ListGeneratorView* ui_;

    ListGeneratorModel* model_;

    WorkerThread* workerThread_;

    void loadTypesToListWidget();

    void loadWhitelistToListWidget();

    void loadElementsToListWidget();

};

#endif // !LISTGENERATORCONTROLLER_H
