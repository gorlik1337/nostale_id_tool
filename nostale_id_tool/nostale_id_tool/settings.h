#ifndef SETTINGS_H
#define SETTINGS_H

#include <QString>
#include <QVector>
#include <design.h>

class Language;

class Settings {
public:
    ~Settings();

    void initialize();

    Language* getLanguage(const QString& abbreviation);

    const QVector<Language*>& getLanguages() const;

    void setLanguages(const QVector<Language*>& value);

    const Design getDesign() const;

    void setDesign(Design value);

    Language* getDefaultLanguage();

    Language* getCurrentLanguage();

    void setCurrentLanguage(Language* value);

    const QString getDataFilePath() const;

    void setDataFilePath(const QString& value);

    const QString getLangFilePath() const;

    void setLangFilePath(const QString& value);

    void save();

    const bool isInitialStart() const;

    static Settings* getInstance();

private:
    Settings();

    static Settings instance_;

    QVector<Language*> languages_;

    Language* currentLanguage_;

    Design design_;

    QString dataFilePath_;

    QString langFilePath_;

    bool isInitialStart_;
};

#endif // !SETTINGS_H
