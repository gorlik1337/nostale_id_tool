#include "settingsparser.h"
#include "settings.h"
#include "language.h"
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>

SettingsParser::SettingsParser(const QString& filepath) :
    filepath_(filepath)
{}

void SettingsParser::parse()
{
    QFile file(filepath_);
    if (!file.open(QIODevice::ReadOnly)) {
        // TODO: error
        return;
    }

    QJsonDocument jsonDocument = QJsonDocument::fromJson(file.readAll());
    file.close();
    assignValues(jsonDocument.object());
}

void SettingsParser::assignValues(const QJsonObject& jsonObject)
{
    auto settings = Settings::getInstance();
    settings->setDataFilePath(jsonObject["dataFilePath"].toString());
    settings->setLangFilePath(jsonObject["langFilePath"].toString());
    auto language = settings->getLanguage(jsonObject["language"].toString());
    if (language != nullptr) {
        settings->setCurrentLanguage(language);
    }
    settings->setDesign(parseDesign(jsonObject["design"].toString()));
}

Design SettingsParser::parseDesign(const QString& design)
{
    return QString(design).toLower() == "dark" ? Design::Dark : Design::Light;
}
