#include "startview.h"
#include "startcontroller.h"

StartView::StartView() :
    controller_(new StartController(this, &ui_))
{
    ui_.setupUi(this);
}

StartView::~StartView()
{}

void StartView::initialize()
{
    // TODO: uncomment this line
    // controller_->initialize();
}

void StartView::resizeEvent(QResizeEvent* resizeEvent)
{
    QWidget::resizeEvent(resizeEvent);
    // TODO: Do some resizing stuff
}

void StartView::retranslate()
{
    controller_->retranslate();
}

void StartView::redesign()
{
    controller_->redesign();
}
