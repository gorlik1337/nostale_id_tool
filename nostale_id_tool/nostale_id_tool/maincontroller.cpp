#include "maincontroller.h"
#include "mainmodel.h"
#include "mainview.h"
#include "ui_mainview.h"
#include "itemeditorview.h"
#include "language.h"
#include "listgeneratorview.h"
#include "retranslatable.h"
#include "startview.h"
#include "settingsview.h"
#include "settings.h"
#include "stylesheet.h"
#include <QDesktopServices>
#include <QResizeEvent>
#include <QRect>

MainController::MainController(MainView* view, Ui::MainView* ui) :
    view_(view),
    ui_(ui),
    model_(new MainModel(this)),
    startView_(new StartView()),
    listGeneratorView_(new ListGeneratorView(&workerThread_)),
    itemEditorView_(new ItemEditorView()),
    settingsView_(new SettingsView(this))
{
    workerThread_.start();
}

MainController::~MainController()
{
    workerThread_.setIsRunning(false);
    /* Wait for thread until finished */
    do {
        QThread::msleep(10);
    } while (workerThread_.isBusy());

    delete model_;
    delete startView_;
    delete listGeneratorView_;
    delete itemEditorView_;
    delete settingsView_;
}

void MainController::initialize()
{
    /* Initialize the stack widget */
    ui_->stackedWidget->addWidget(startView_);
    ui_->stackedWidget->addWidget(listGeneratorView_);
    ui_->stackedWidget->addWidget(itemEditorView_);
    ui_->stackedWidget->addWidget(settingsView_);
    ui_->stackedWidget->removeWidget(ui_->page);

    // Read types here

    startView_->initialize();
    listGeneratorView_->initialize();
    itemEditorView_->initialize();
    settingsView_->initialize();

    retranslate();
}

void MainController::resizeEvent(QResizeEvent* resizeEvent)
{
    /* Center navigation board */
    auto geometry = ui_->navigationWidget->geometry();
    const auto posX = 0.5F * (view_->width() - geometry.width());
    ui_->navigationWidget->move(posX, geometry.y());

    /* Move and resize stackedWidget */
    geometry = QRect();
    geometry.setY(ui_->navigationWidget->height());
    geometry.setWidth(view_->width());
    geometry.setHeight(view_->height() - ui_->navigationWidget->height());
    ui_->stackedWidget->setGeometry(geometry);

    startView_->resizeEvent(resizeEvent);
    listGeneratorView_->resizeEvent(resizeEvent);
    itemEditorView_->resizeEvent(resizeEvent);
    settingsView_->resizeEvent(resizeEvent);
}

void MainController::navigate(const NavigationPage navigationPage)
{
    /* Navigate to forum thread */
    if (navigationPage == NavigationPage::Credits) {
        QDesktopServices::openUrl(QUrl("https://bit.ly/2ve0EAa"));
    }
    else {
        ui_->stackedWidget->setCurrentIndex(static_cast<int>(navigationPage));
    }
}

void MainController::retranslate()
{
    auto currentLanguage = Settings::getInstance()->getCurrentLanguage();
    ui_->startPushButton->setText(currentLanguage->getValue("navigation.start"));
    ui_->generateListsPushButton->setText(currentLanguage->getValue("navigation.listGenerator"));
    ui_->editItemsPushButton->setText(currentLanguage->getValue("navigation.itemEditor"));
    ui_->settingsPushButton->setText(currentLanguage->getValue("navigation.settings"));

    /* Retranslate further UI elements */
    startView_->retranslate();
    listGeneratorView_->retranslate();
    itemEditorView_->retranslate();
    settingsView_->retranslate();
}

void MainController::redesign()
{
    /* Redesign UI and the navigation elements */
    Design design = Settings::getInstance()->getDesign();
    view_->setStyleSheet(Stylesheet::getMain(design));
    ui_->startPushButton->setStyleSheet(Stylesheet::getNavigation(design));
    ui_->generateListsPushButton->setStyleSheet(Stylesheet::getNavigation(design));
    ui_->editItemsPushButton->setStyleSheet(Stylesheet::getNavigation(design));
    ui_->settingsPushButton->setStyleSheet(Stylesheet::getNavigation(design));
    ui_->creditsPushButton->setStyleSheet(Stylesheet::getCredits(design));
    ui_->navigationWidget->setStyleSheet(Stylesheet::getWidget(design));

    /* Redesign further UI elements */
    startView_->redesign();
    listGeneratorView_->redesign();
    itemEditorView_->redesign();
    settingsView_->redesign();
}
