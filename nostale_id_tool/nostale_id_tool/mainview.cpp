#include "mainview.h"
#include "maincontroller.h"
#include "navigationpage.h"

MainView::MainView() :
    controller_(new MainController(this, &ui_))
{
    ui_.setupUi(this);

    /* Prevent stretching the window smaller */
    setMinimumSize(size());

    controller_->initialize();

    /* Navigate to start page initially */
    controller_->navigate(NavigationPage::Start);
}

MainView::~MainView()
{
    delete controller_;
}

void MainView::startPushButtonClicked()
{
    controller_->navigate(NavigationPage::Start);
}

void MainView::generateListsPushButtonClicked()
{
    controller_->navigate(NavigationPage::ListCreator);
}

void MainView::editItemsPushButtonClicked()
{
    controller_->navigate(NavigationPage::ItemEditor);
}

void MainView::settingsPushButtonClicked()
{
    controller_->navigate(NavigationPage::Settings);
}

void MainView::creditsPushButtonClicked()
{
    controller_->navigate(NavigationPage::Credits);
}

void MainView::resizeEvent(QResizeEvent* resizeEvent)
{
    QWidget::resizeEvent(resizeEvent);
    controller_->resizeEvent(resizeEvent);
}
