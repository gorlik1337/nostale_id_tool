#ifndef FILETYPE_H
#define FILETYPE_H

#include <QString>

struct FileType {
    QString name;

    static FileType txt;

    static FileType csv;

    static FileType html;

private:
    FileType(const QString& name) :
        name(name)
    {}
};

#endif // !FILETYPE_H
