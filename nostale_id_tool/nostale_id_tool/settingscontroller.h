#ifndef SETTINGSCONTROLLER_H
#define SETTINGSCONTROLLER_H

#include <QObject>
#include "design.h"
#include "retranslatable.h"

class QResizeEvent;
class SettingsView;
namespace Ui {
    class SettingsView;
}
class SettingsModel;
class Settings;
class MainController;

class SettingsController : public QObject, public Retranslatable {
    Q_OBJECT

public:
    SettingsController(SettingsView* view, Ui::SettingsView* ui, MainController* mainController);

    ~SettingsController();

    void initialize();

    void resizeEvent(QResizeEvent* resizeEvent);

    void retranslate() override;

    void changeDataFilePath();

    void changeLangFilePath();

    void changeLanguage(int languageIndex);

    void changeDesign(Design design);

    void saveSettings();

    void saveDataFilePaths();

private:
    SettingsView* view_;

    Ui::SettingsView* ui_;

    SettingsModel* model_;

    Settings* settings_;

    MainController* mainController_;

    const QString getOpenFileName() const;

#ifdef Q_OS_WIN
    const QStringList getProbableFilePaths() const;

    void suggestProbableFilePaths(const QStringList& filepaths);
#endif // Q_OS_WIN
};

#endif // !SETTINGSCONTROLLER_H
