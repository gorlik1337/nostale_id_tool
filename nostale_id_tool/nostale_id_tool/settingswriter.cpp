#include "settingswriter.h"
#include <QFile>

SettingsWriter::SettingsWriter(const QString& filepath)
{
    file_.setFileName(filepath);

    /* Override */
    if (!file_.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text)) {
        // TODO: error
        return;
    }
}

SettingsWriter::~SettingsWriter()
{}

void SettingsWriter::write(const QByteArray& textBytes)
{
    file_.write(textBytes);
}
