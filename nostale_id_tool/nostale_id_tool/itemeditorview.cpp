#include "itemeditorview.h"

ItemEditorView::ItemEditorView()
{
    ui_.setupUi(this);
}

ItemEditorView::~ItemEditorView()
{}

void ItemEditorView::initialize()
{
    // TODO: uncomment this line
    // controller_->initialize();
}

void ItemEditorView::resizeEvent(QResizeEvent* resizeEvent)
{
    QWidget::resizeEvent(resizeEvent);
    // TODO: Do some resizing stuff
}

void ItemEditorView::retranslate()
{
    // TODO: uncomment
    // controller_->retranslate();
}

void ItemEditorView::redesign()
{
    // TODO: uncomment this line
    // controller_->redesign();
}
