#ifndef TYPE_H
#define TYPE_H

#include <QString>
#include <QVector>
#include "elemententry.h"
#include "whitelistentry.h"

struct Type {
    QString name;

    QString dataFileName;

    QString langFileName;

    QVector<WhitelistEntry> whitelist;

    QVector<ElementEntry> elements;

    bool isWhitelistActive;
};

#endif // !TYPE_H
