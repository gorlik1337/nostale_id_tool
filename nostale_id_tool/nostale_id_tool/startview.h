#ifndef STARTVIEW_H
#define STARTVIEW_H

#include <QWidget>
#include "redesignable.h"
#include "retranslatable.h"
#include "ui_startview.h"

class StartController;

class StartView : public QWidget, public Retranslatable, public Redesignable {
    Q_OBJECT

public:
    StartView();

    ~StartView();

    void initialize();

    void resizeEvent(QResizeEvent* resizeEvent);

    void retranslate() override;

    void redesign() override;

private:
    StartController* controller_;

    Ui::StartView ui_;
};

#endif // !STARTVIEW_H
