#ifndef MAINCONTROLLER_H
#define MAINCONTROLLER_H

#include <QObject>
#include "navigationpage.h"
#include "retranslatable.h"
#include "redesignable.h"
#include "workerthread.h"

class MainModel;
class MainView;
namespace Ui {
    class MainView;
}
class QResizeEvent;
class StartView;
class ListGeneratorView;
class ItemEditorView;
class SettingsView;

class MainController : public QObject, public Retranslatable, public Redesignable {
    Q_OBJECT

public:
    MainController(MainView* view, Ui::MainView* ui);

    ~MainController();

    void initialize();

    void resizeEvent(QResizeEvent* resizeEvent);

    void navigate(const NavigationPage navigationPage);

    void retranslate() override;

    void redesign() override;

private:
    MainModel* model_;

    MainView* view_;

    Ui::MainView* ui_;

    StartView* startView_;

    ListGeneratorView* listGeneratorView_;

    ItemEditorView* itemEditorView_;

    SettingsView* settingsView_;

    WorkerThread workerThread_;
};

#endif // !MAINCONTROLLER_H
