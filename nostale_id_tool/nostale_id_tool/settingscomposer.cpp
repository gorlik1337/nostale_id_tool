#include "settingscomposer.h"
#include <QJsonDocument>
#include <QJsonObject>
#include "language.h"
#include "settings.h"

QByteArray SettingsComposer::getJsonData()
{
    Settings* settings = Settings::getInstance();
    QJsonDocument result;
    QJsonObject object;

    const auto design = settings->getDesign();
    QString key = QString("settings.groupBoxUI.design.%1").arg(design == Design::Light ? "light" : "dark");

    object["design"] = settings->getDefaultLanguage()->getValue(key);
    object["language"] = settings->getCurrentLanguage()->getValue("abbreviation");
    object["dataFilePath"] = settings->getDataFilePath();
    object["langFilePath"] = settings->getLangFilePath();

    result.setObject(object);
    return result.toJson();
}
