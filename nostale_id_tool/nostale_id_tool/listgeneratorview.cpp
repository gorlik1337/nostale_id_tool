#include "listgeneratorview.h"
#include "listgeneratorcontroller.h"
#include "elemententry.h"
#include "filetype.h"

ListGeneratorView::ListGeneratorView(WorkerThread* workerThread) :
    controller_(new ListGeneratorController(this, &ui_, workerThread))
{
    ui_.setupUi(this);
}

ListGeneratorView::~ListGeneratorView()
{
    delete controller_;
}

void ListGeneratorView::initialize()
{
    controller_->initialize();
}

void ListGeneratorView::resizeEvent(QResizeEvent* resizeEvent)
{
    QWidget::resizeEvent(resizeEvent);
    controller_->resizeEvent(resizeEvent);
}

void ListGeneratorView::retranslate()
{
    controller_->retranslate();
}

void ListGeneratorView::redesign()
{
    controller_->redesign();
}

void ListGeneratorView::explorePushButtonClicked()
{
    controller_->promptForOutputPath();
}

void ListGeneratorView::showInExplorerPushButtonClicked()
{
    controller_->openOutputPathInExplorer();
}

void ListGeneratorView::addAllPushButtonClicked()
{
    controller_->appendAllElementsToOutputFormat();
}

void ListGeneratorView::clearPushButtonClicked()
{
    controller_->clearOutputFormat();
}

void ListGeneratorView::extractPushButtonClicked()
{
    // TODO: Generate lists dynamically
}

void ListGeneratorView::checkWhitelistEntriesPushButtonClicked()
{
    controller_->setWhitelistCheckState(Qt::Checked);
}

void ListGeneratorView::uncheckWhitelistEntriesPushButtonClicked()
{
    controller_->setWhitelistCheckState(Qt::Unchecked);
}

void ListGeneratorView::whitelistActiveCheckBoxClicked()
{
    controller_->updateIsWhitelistActive();
}

void ListGeneratorView::txtRadioButtonClicked()
{
    controller_->setSelectedFileType(&FileType::txt);
    controller_->setPredefinedOutputPath();
}

void ListGeneratorView::csvRadioButtonClicked()
{
    controller_->setSelectedFileType(&FileType::csv);
    controller_->setPredefinedOutputPath();
}

void ListGeneratorView::htmlRadioButtonClicked()
{
    controller_->setSelectedFileType(&FileType::html);
    controller_->setPredefinedOutputPath();
}

void ListGeneratorView::whitelistListWidgetItemChanged(QListWidgetItem* item)
{
    controller_->updateWhitelistEntryCheckState(controller_->getWhitelistListWidgetItemIndex(item));
}

void ListGeneratorView::typeListWidgetCurrentRowChanged(int index)
{
    controller_->setCurrentType(index);
}

void ListGeneratorView::elementsListWidgetItemClicked(QListWidgetItem* item)
{
    auto element = controller_->getElement(item);
    controller_->appendElementToOutputFormat(element);
}
