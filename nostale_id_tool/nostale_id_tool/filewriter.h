#ifndef FILEWRITER_H
#define FILEWRITER_H

#include <QString>

class FileWriter {
public:
    virtual void write(const QByteArray& textBytes) = 0;

};

#endif // !FILEWRITER_H
