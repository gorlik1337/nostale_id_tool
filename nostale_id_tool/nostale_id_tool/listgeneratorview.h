#ifndef LISTGENERATORVIEW_H
#define LISTGENERATORVIEW_H

#include <QWidget>
#include "redesignable.h"
#include "retranslatable.h"
#include "ui_listgeneratorview.h"

class ListGeneratorController;
class WorkerThread;

class ListGeneratorView : public QWidget, public Retranslatable, public Redesignable {
    Q_OBJECT

public:
    ListGeneratorView(WorkerThread* workerThread);

    ~ListGeneratorView();

    void initialize();

    void resizeEvent(QResizeEvent* resizeEvent);

    void retranslate() override;

    void redesign() override;

private:
    Ui::ListGeneratorView ui_;

    ListGeneratorController* controller_;

private slots:
    void explorePushButtonClicked();

    void showInExplorerPushButtonClicked();

    void addAllPushButtonClicked();

    void clearPushButtonClicked();

    void extractPushButtonClicked();

    void checkWhitelistEntriesPushButtonClicked();

    void uncheckWhitelistEntriesPushButtonClicked();

    void whitelistActiveCheckBoxClicked();

    void txtRadioButtonClicked();

    void csvRadioButtonClicked();

    void htmlRadioButtonClicked();

    void whitelistListWidgetItemChanged(QListWidgetItem* item);

    void typeListWidgetCurrentRowChanged(int index);

    void elementsListWidgetItemClicked(QListWidgetItem* item);
};

#endif // !LISTGENERATORVIEW_H
