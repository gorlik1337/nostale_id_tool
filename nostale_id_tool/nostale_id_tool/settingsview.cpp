#include "settingsview.h"
#include "maincontroller.h"
#include "settingscontroller.h"

SettingsView::SettingsView(MainController* mainController) :
    controller_(new SettingsController(this, &ui_, mainController))
{
    ui_.setupUi(this);
}

SettingsView::~SettingsView()
{
    delete controller_;
}

void SettingsView::initialize()
{
    controller_->initialize();
}

void SettingsView::resizeEvent(QResizeEvent* resizeEvent)
{
    QWidget::resizeEvent(resizeEvent);
    controller_->resizeEvent(resizeEvent);
}

void SettingsView::retranslate()
{
    controller_->retranslate();
}

void SettingsView::redesign()
{
    // TODO: uncomment this line
    // controller_->redesign();
}

void SettingsView::languageComboBoxCurrentIndexChanged(int currentIndex)
{
    // TODO: nothing (?)
}

void SettingsView::continuePushButtonClicked()
{
    controller_->changeLanguage(ui_.languageComboBox->currentIndex());
    controller_->saveSettings();
}

void SettingsView::lightRadioButtonClicked()
{
    controller_->changeDesign(Design::Light);
    controller_->saveSettings();
}

void SettingsView::darkRadioButtonClicked()
{
    controller_->changeDesign(Design::Dark);
    controller_->saveSettings();
}

void SettingsView::exploreDataFilePushButtonClicked()
{
    controller_->changeDataFilePath();
}

void SettingsView::exploreLangFilePushButtonClicked()
{
    controller_->changeLangFilePath();
}

void SettingsView::savePushButtonClicked()
{
    controller_->saveDataFilePaths();
    controller_->saveSettings();
}
