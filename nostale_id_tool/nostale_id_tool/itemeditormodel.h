#ifndef ITEMEDITORMODEL_H
#define ITEMEDITORMODEL_H

#include <QObject>

class ItemEditorModel : public QObject {
    Q_OBJECT

public:
    ItemEditorModel();

    ~ItemEditorModel();
};

#endif // !ITEMEDITORMODEL_H
