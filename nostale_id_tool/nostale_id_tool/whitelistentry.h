#ifndef WHITELISTENTRY_H
#define WHITELISTENTRY_H

#include <QString>

struct WhitelistEntry {
    QString name;

    bool isChecked;

    WhitelistEntry() :
        name(),
        isChecked(false)
    {}

    WhitelistEntry(QString name) :
        name(name),
        isChecked(false)
    {}
};

#endif // !WHITELISTENTRY_H
