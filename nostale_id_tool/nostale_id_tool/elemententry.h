#ifndef ELEMENTENTRY_H
#define ELEMENTENTRY_H

#include <QString>

struct ElementEntry {
    QString name;

    // TODO: add dynamic parameters here

    ElementEntry() {}

    ElementEntry(const QString& name) :
        name(name)
    {}
};

#endif // !ELEMENTENTRY_H
