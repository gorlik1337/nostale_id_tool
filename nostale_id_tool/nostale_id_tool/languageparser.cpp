#include "languageparser.h"
#include "language.h"

#include <QDir>
#include <QMap>
#include <QVector>
#include <QJsonDocument>
#include <QJsonValue>

// TODO refactor prefixes

LanguageParser::LanguageParser(const QString& directory) :
    directory_(directory)
{}

QVector<Language*> LanguageParser::parse()
{
    /* Get all filenames contained in directory */
    const auto files = QDir(directory_).entryList(QStringList() << "*.json", QDir::Files);

    QVector<Language*> result;
    result.reserve(files.size());

    /* Parse each language */
    for (QString filename : files) {
        result.append(readLanguageData(directory_ + filename));
    }
    return result;
}

Language* LanguageParser::readLanguageData(const QString & filename)
{
    QFile file;
    file.setFileName(filename);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        // TODO: error
        return nullptr;
    }

    const auto fileContent = file.readAll();
    file.close();
    return parseLanguageData(fileContent);
}

Language* LanguageParser::parseLanguageData(const QString & jsonString)
{
    QMap<QString, QString> resultMap;

    QJsonDocument jsonDocument = QJsonDocument::fromJson(jsonString.toUtf8());
    QJsonObject jsonObject = jsonDocument.object();

    /* General information */
    resultMap["language"] = jsonObject.value("language").toString();
    resultMap["abbreviation"] = jsonObject.value("abbreviation").toString();

    /* Translation data */
    const auto data = jsonObject.value("data").toObject();

    parseStartData(resultMap, data);
    parseNavigationData(resultMap, data);
    parseListGeneratorData(resultMap, data);
    parseSettingsData(resultMap, data);
    return new Language(resultMap);
}

void LanguageParser::parseNavigationData(QMap<QString, QString> & resultMap, const QJsonObject & jsonObject)
{
    const auto nodeObject = jsonObject.value("navigation").toObject();
    const QString prefix = "navigation.";
    resultMap[prefix + "start"] = nodeObject.value("start").toString();
    resultMap[prefix + "listGenerator"] = nodeObject.value("listGenerator").toString();
    resultMap[prefix + "itemEditor"] = nodeObject.value("itemEditor").toString();
    resultMap[prefix + "settings"] = nodeObject.value("settings").toString();
}

void LanguageParser::parseStartData(QMap<QString, QString> & resultMap, const QJsonObject & jsonObject)
{
    const auto nodeJsonObject = jsonObject.value("start").toObject();
    const QString prefix = "start.";
    resultMap[prefix + "welcome"] = nodeJsonObject.value("welcome").toString();
    resultMap[prefix + "welcomeBack"] = nodeJsonObject.value("welcomeBack").toString();
}

void LanguageParser::parseListGeneratorData(QMap<QString, QString> & resultMap, const QJsonObject & jsonObject)
{
    const auto rootJsonObject = jsonObject.value("listGenerator").toObject();
    const QString prefix = "listGenerator.";
#pragma region groupBoxOutputFormat
    {
        const auto tempJsonObject = rootJsonObject.value("groupBoxOutputFormat").toObject();
        const auto tempPrefix = prefix + "groupBoxOutputFormat.";
        resultMap[tempPrefix + "title"] = tempJsonObject.value("title").toString();
        resultMap[tempPrefix + "label"] = tempJsonObject.value("label").toString();
    }
#pragma endregion

#pragma region groupBoxOutputPath
    {
        const auto tempJsonObject = rootJsonObject.value("groupBoxOutputPath").toObject();
        const auto tempPrefix = prefix + "groupBoxOutputPath.";
        resultMap[tempPrefix + "title"] = tempJsonObject.value("title").toString();
        resultMap[tempPrefix + "label"] = tempJsonObject.value("label").toString();
        resultMap[tempPrefix + "exploreButton"] = tempJsonObject.value("exploreButton").toString();
        resultMap[tempPrefix + "showInExplorerButton"] = tempJsonObject.value("showInExplorerButton").toString();
        resultMap[tempPrefix + "txtFile"] = tempJsonObject.value("txtFile").toString();
        resultMap[tempPrefix + "csvFile"] = tempJsonObject.value("csvFile").toString();
        resultMap[tempPrefix + "htmlFile"] = tempJsonObject.value("htmlFile").toString();
    }
#pragma endregion

#pragma region groupBoxWhitelist
    {
        const auto tempJsonObject = rootJsonObject.value("groupBoxWhitelist").toObject();
        const auto tempPrefix = prefix + "groupBoxWhitelist.";
        resultMap[tempPrefix + "title"] = tempJsonObject.value("title").toString();
        resultMap[tempPrefix + "label"] = tempJsonObject.value("label").toString();
        resultMap[tempPrefix + "checkButton"] = tempJsonObject.value("checkButton").toString();
        resultMap[tempPrefix + "uncheckButton"] = tempJsonObject.value("uncheckButton").toString();
        resultMap[tempPrefix + "whitelistActive"] = tempJsonObject.value("whitelistActive").toString();
    }
#pragma endregion

#pragma region groupBoxType
    {
        const auto tempJsonObject = rootJsonObject.value("groupBoxType").toObject();
        const auto tempPrefix = prefix + "groupBoxType.";
        resultMap[tempPrefix + "title"] = tempJsonObject.value("title").toString();
        resultMap[tempPrefix + "label"] = tempJsonObject.value("label").toString();
    }
#pragma endregion

#pragma region Available elements
    resultMap[prefix + "labelElementsAvailable"] = rootJsonObject.value("labelElementsAvailable").toString();
    resultMap[prefix + "buttonAddAll"] = rootJsonObject.value("buttonAddAll").toString();
    resultMap[prefix + "buttonClear"] = rootJsonObject.value("buttonClear").toString();
    resultMap[prefix + "buttonExtract"] = rootJsonObject.value("buttonExtract").toString();
#pragma endregion
}

void LanguageParser::parseSettingsData(QMap<QString, QString> & resultMap, const QJsonObject & jsonObject)
{
    const auto rootJsonObject = jsonObject.value("settings").toObject();
    const QString prefix = "settings.";

#pragma region groupBoxUI
    {
        const auto tempJsonObject = rootJsonObject.value("groupBoxUI").toObject();
        auto tempPrefix = prefix + "groupBoxUI.";
        resultMap[tempPrefix + "title"] = "Id Tool " + tempJsonObject.value("title").toString();

        const auto designJsonObject = tempJsonObject.value("design").toObject();
        tempPrefix += "design.";
        resultMap[tempPrefix + "title"] = designJsonObject.value("title").toString();
        resultMap[tempPrefix + "light"] = designJsonObject.value("light").toString();
        resultMap[tempPrefix + "dark"] = designJsonObject.value("dark").toString();
    }
#pragma endregion

#pragma region groupBoxListGenerator
    {
        const auto tempJsonObject = rootJsonObject.value("groupBoxListGenerator").toObject();
        const auto tempPrefix = prefix + "groupBoxListGenerator.";
        resultMap[tempPrefix + "title"] = tempJsonObject.value("title").toString();
        resultMap[tempPrefix + "dataFile"] = tempJsonObject.value("dataFile").toString();
        resultMap[tempPrefix + "langFile"] = tempJsonObject.value("langFile").toString();
        resultMap[tempPrefix + "saveButton"] = tempJsonObject.value("saveButton").toString();
    }
#pragma endregion

}
