#ifndef ITEMEDITORVIEW_H
#define ITEMEDITORVIEW_H

#include <QWidget>
#include "redesignable.h"
#include "retranslatable.h"
#include "ui_itemeditorview.h"

class ItemEditorView : public QWidget, public Retranslatable, public Redesignable {
    Q_OBJECT

public:
    ItemEditorView();

    ~ItemEditorView();

    void initialize();

    void resizeEvent(QResizeEvent* resizeEvent);

    void retranslate() override;

    void redesign() override;

private:
    Ui::ItemEditorView ui_;
};

#endif // !ITEMEDITORVIEW_H
