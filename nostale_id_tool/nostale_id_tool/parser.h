#ifndef PARSER_H
#define PARSER_H

template<class T>
class Parser {
public:
    virtual T parse() = 0;
};

#endif // !PARSER_H
