#ifndef LANGUAGEPARSER_H
#define LANGUAGEPARSER_H

#include <QObject>
#include <QJsonObject>
#include <QVector>
#include "parser.h"

class Language;

class LanguageParser : public QObject, public Parser<QVector<Language*>> {
    Q_OBJECT

public:
    LanguageParser(const QString& directory);

    QVector<Language*> parse() override;

private:
    QString directory_;

    Language* readLanguageData(const QString& filename);

    Language* parseLanguageData(const QString& jsonString);

    void parseNavigationData(QMap<QString, QString>& resultMap, const QJsonObject& jsonObject);

    void parseStartData(QMap<QString, QString>& resultMap, const QJsonObject& jsonObject);

    void parseListGeneratorData(QMap<QString, QString>& resultMap, const QJsonObject& jsonObject);

    void parseSettingsData(QMap<QString, QString>& resultMap, const QJsonObject& jsonObject);
};

#endif // !LANGUAGEPARSER_H
