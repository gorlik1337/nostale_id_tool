#ifndef DESIGN_H
#define DESIGN_H

enum class Design : char {
    Light,
    Dark
};

#endif // !DESIGN_H
