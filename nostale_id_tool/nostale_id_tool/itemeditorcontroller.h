#ifndef ITEMEDITORCONTROLLER_H
#define ITEMEDITORCONTROLLER_H

#include <QObject>
#include "redesignable.h"
#include "retranslatable.h"

class ItemEditorController : public QObject, public Retranslatable, public Redesignable {
    Q_OBJECT

public:
    ItemEditorController();

    ~ItemEditorController();

    void retranslate() override;

    void redesign() override;
};

#endif // !ITEMEDITORCONTROLLER_H
