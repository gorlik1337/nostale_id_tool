#include "startcontroller.h"
#include "language.h"
#include "settings.h"
#include "startview.h"
#include "startmodel.h"

StartController::StartController(StartView* view, Ui::StartView* ui) :
    view_(view),
    ui_(ui)
{}

StartController::~StartController()
{}

void StartController::retranslate()
{
    auto settings = Settings::getInstance();
    auto currentLanguage = settings->getCurrentLanguage();
    if (settings->isInitialStart()) {
        ui_->welcomeLabel->setText(currentLanguage->getValue("start.welcome"));
    }
    else {
        ui_->welcomeLabel->setText(currentLanguage->getValue("start.welcomeBack"));
    }
    // TODO: retranslate UI
}

void StartController::redesign()
{
    // TODO: redesign UI
}
