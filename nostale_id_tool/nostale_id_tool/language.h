#ifndef LANGUAGE_H
#define LANGUAGE_H

#include <QObject>
#include <QMap>

class Language : public QObject {
public:
    Language(const QMap<QString, QString>& data);

    const QString getValue(const QString& key) const;

private:
    QMap<QString, QString> data_;

};

#endif // !LANGUAGE_H
