#ifndef SETTINGSPARSER_H
#define SETTINGSPARSER_H

#include <QObject>
#include <QJsonObject>
#include "design.h"
#include "parser.h"
#include "settings.h"

class Language;

class SettingsParser : public QObject, public Parser<void> {
    Q_OBJECT

public:
    SettingsParser(const QString& filepath);

    ~SettingsParser() = default;

    void parse() override;

private:
    QString filepath_;

    void assignValues(const QJsonObject& jsonObject);

    static Design parseDesign(const QString& design);
};

#endif // !SETTINGSPARSER_H
