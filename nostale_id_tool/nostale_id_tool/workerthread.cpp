#include "workerthread.h"

WorkerThread::WorkerThread() :
    isRunning_(static_cast<int>(true)),
    isBusy_(static_cast<int>(false))
{}

WorkerThread::~WorkerThread()
{}

void WorkerThread::setIsRunning(bool isRunning)
{
    isRunning_ = static_cast<int>(isRunning);
}

bool WorkerThread::isBusy()
{
    return isBusy_;
}

void WorkerThread::run()
{
    while (isRunning_ == true) {
        QThread::msleep(5);
    }
}
