#ifndef STYLESHEET_H
#define STYLESHEET_H

#include <QObject>
#include "design.h"

class Stylesheet : public QObject {
    Q_OBJECT

public:
    static QString getMain(Design design);

    static QString getNavigation(Design design);

    static QString getCredits(Design design);

    static QString getWidget(Design design);
};

#endif // !STYLESHEET_H
