#ifndef MAINMODEL_H
#define MAINMODEL_H

#include <QObject>

class MainController;

class MainModel : public QObject {
    Q_OBJECT

public:
    MainModel(MainController* controller);

    ~MainModel();

private:
    MainController* controller_;
};

#endif // !MAINMODEL_H
