#ifndef MAINVIEW_H
#define MAINVIEW_H

#include <QWidget>
#include "ui_mainview.h"

class MainController;

class MainView : public QWidget {
    Q_OBJECT

public:
    MainView();

    ~MainView();

private:
    Ui::MainView ui_;

    MainController* controller_;

private slots:
    void startPushButtonClicked();

    void generateListsPushButtonClicked();

    void editItemsPushButtonClicked();

    void settingsPushButtonClicked();

    void creditsPushButtonClicked();

    void resizeEvent(QResizeEvent* resizeEvent) override;

};

#endif // !MAINVIEW_H
