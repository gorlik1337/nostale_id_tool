#ifndef LISTGENERATORMODEL_H
#define LISTGENERATORMODEL_H

#include <QObject>
#include <QList>
#include <QVector>
#include "filetype.h"
#include "type.h"

class ListGeneratorController;

class ListGeneratorModel : public QObject {
    Q_OBJECT

public:
    ListGeneratorModel(ListGeneratorController* controller);

    ~ListGeneratorModel();

    void initialize();

    const QStringList getTypeNames() const;

    Type* getCurrentType();

    void setCurrentType(int index);

    const QStringList getWhitelist() const;

    const QStringList getElements() const;

    void appendToSelectedElements(ElementEntry* element);

    void clearSelectedElements();

    FileType* getSelectedFileType();

    void setSelectedFileType(FileType* fileType);

private:
    ListGeneratorController* controller_;

    QVector<Type> types_;

    Type* currentType_;

    QList<ElementEntry*> selectedElements_;

    FileType* selectedFileType_;
};

#endif // !LISTGENERATORMODEL_H
