#include "stylesheet.h"

// TODO: refactor

QString Stylesheet::getMain(Design design)
{
    if (design == Design::Light) {
        return "MainView { background-color: white; }";
    }
    else {
        return "*{background:#191919;color:#DDDDDD;border:1px solid #5A5A5A;}QWidget::item:selected{background:#3D7"
            "848;} QCheckBox, QRadioButton{border:none;}QRadioButton::indicator, QCheckBox::indicator{width:13px"
            ";height:13px;}QRadioButton::indicator::unchecked, QCheckBox::indicator::unchecked{border:1px solid#"
            "5A5A5A;background:none;} QRadioButton::indicator:unchecked:hover,QCheckBox::indicator:unchecked:hov"
            "er{border:1px solid #DDDDDD;} QRadioButton::indicator::checked,QCheckBox::indicator::checked{border"
            ":1px solid #5A5A5A;background:#5A5A5A;}QRadioButton::indicator:checked:hover, QCheckBox::indicator:"
            "checked:hover{border:1px solid#DDDDDD;background:#DDDDDD;} QGroupBox{margin-top:6px;} QGroupBox::ti"
            "tle{top:-7px;left:7px;}QScrollBar{border:1px solid #5A5A5A;background:#191919;}QScrollBar:horizonta"
            "l{height:15px;margin:0px 0px 0px 32px;}QScrollBar:vertical{width:15px;margin:32px 0px 0px 0px;}QScr"
            "ollBar::handle{background:#353535;border:1px solid #5A5A5A;}QScrollBar::handle:horizontal{border-wi"
            "dth:0px 1px 0px 1px;}QScrollBar::handle:vertical{border-width:1px 0px 1px 0px;}QScrollBar::handle:h"
            "orizontal{min-width:20px;} QScrollBar::handle:vertical{min-height:20px;}QScrollBar::add-line, QScro"
            "llBar::sub-line{background:#353535;border:1px solid#5A5A5A;subcontrol-origin:margin;} QScrollBar::a"
            "dd-line{position:absolute;}QScrollBar::add-line:horizontal{width:15px;subcontrol-position:left;left"
            ":15px;}QScrollBar::add-line:vertical{height:15px;subcontrol-position:top;top:15px;}QScrollBar::sub-"
            "line:horizontal{width:15px;subcontrol-position:top left;}QScrollBar::sub-line:vertical{height:15px;"
            "subcontrol-position:top;} QScrollBar:left-arrow,QScrollBar::right-arrow, QScrollBar::up-arrow, QScr"
            "ollBar::down-arrow{border:1px solid#5A5A5A;width:3px;height:3px;} QScrollBar::add-page, QScrollBar:"
            ":sub-page{background:none;}QAbstractButton:hover{background:#353535;} QAbstractButton:pressed{backg"
            "round:#191919;}QAbstractItemView{show-decoration-selected:1;selection-background-color:#3D7848;sele"
            "ction-color:#DDDDDD;alternate-background-color:#353535;}QHeaderView{border:1px solid #5A5A5A;} QHea"
            "derView::section{background:#191919;border:1px solid#5A5A5A;padding:4px;} QHeaderView::section:sele"
            "cted,QHeaderView::section::checked{background:#353535;} QTableView{gridline-color:#5A5A5A;}QTabBar{"
            "margin-left:2px;} QTabBar::tab{border-radius:0px;padding:4px;margin:4px;}QTabBar::tab:selected{back"
            "ground:#353535;} QComboBox::down-arrow{border:1px solid#5A5A5A;background:#353535;} QComboBox::drop"
            "-down{border:1px solid#5A5A5A;background:#191919;color:#DDDDDD;} QComboBox::down-arrow{width:3px;he"
            "ight:3px;border:1pxsolid #191919;} QAbstractSpinBox{padding-right:15px;} QAbstractSpinBox::up-butto"
            "n,QAbstractSpinBox::down-button{border:1px solid#5A5A5A;background:#353535;subcontrol-origin:border"
            ";} QAbstractSpinBox::up-arrow,QAbstractSpinBox::down-arrow{width:3px;height:3px;border:1px solid #5"
            "A5A5A;} QSlider{border:none;}QSlider::groove:horizontal{height:5px;margin:4px 0px 4px 0px;}QSlider:"
            ":groove:vertical{width:5px;margin:0px 4px 0px 4px;} QSlider::handle{border:1px solid#5A5A5A;backgro"
            "und:#353535;} QSlider::handle:horizontal{width:15px;margin:-4px 0px -4px 0px;}QSlider::handle:verti"
            "cal{height:15px;margin:0px -4px 0px -4px;} QSlider::add-page:vertical,QSlider::sub-page:horizontal{"
            "background:#3D7848;} QSlider::sub-page:vertical,QSlider::add-page:horizontal{background:#353535;} Q"
            "ProgressBar{text-align:center;}QProgressBar::chunk{width:1px;background-color:#3D7848;} QMenu::sepa"
            "rator{background:#353535;}QPushButton{background:#191919;color:#DDDDDD;}QLabel{background:#191919;c"
            "olor:#DDDDDD;border:none;} QLineEdit{background:#191919;color:#DDDDDD;}QComboBox{background-image:u"
            "rl(a);background:#191919;color:#DDDDDD;};";
    }
}

QString Stylesheet::getNavigation(Design design)
{
    if (design == Design::Light) {
        return "QPushButton { background-color: transparent; font-weight: bold; text-decoration: none; }"
            "QPushButton:hover:!pressed { text-decoration: underline; }";
    }
    else {
        return "QPushButton { font-weight: bold; }"
            "QPushButton:hover { background-color: transparent; }"
            "QPushButton:hover:!pressed { text-decoration: underline; }";
    }
}

QString Stylesheet::getCredits(Design design)
{
    if (design == Design::Light) {
        return "QPushButton { background-color: transparent; font-weight: bold; color: blue; text-decoration: under"
            "line }";
    }
    else {
        return "QPushButton { color: dodgerblue; text-decoration: underline; font-weight: bold; } QPushButton:hover"
            "{ background-color: transparent; }";
    }
}

QString Stylesheet::getWidget(Design design)
{
    if (design == Design::Light) {
        return "";
    }
    else {
        return "QWidget { border: none; }";
    }
}
