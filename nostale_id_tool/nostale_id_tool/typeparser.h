#ifndef TYPEPARSER_H
#define TYPEPARSER_H

#include <QObject>
#include <QVector>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include "parser.h"
#include "type.h"

class TypeParser : public QObject, public Parser<QVector<Type>> {
    Q_OBJECT

public:
    TypeParser(const QString& filepath);

    ~TypeParser();

    QVector<Type> parse() override;

private:
    QString filepath_;

    Type parseType(const QString& type, const QJsonObject& object);

    template<typename T, typename S>
    void parseArrayToList(QJsonArray& jsonArray, QVector<T>& list);
};

template<typename T, typename S>
inline void TypeParser::parseArrayToList(QJsonArray& jsonArray, QVector<T>& list)
{
    QVector<S> tempList;
    for (auto jsonValue : jsonArray) {
        if (jsonValue.isString()) {
            const auto name = jsonValue.toString();
            if (!tempList.contains(name) && !name.simplified().isEmpty()) {
                list.append(T(name));
                tempList.append(name);
            }
        }
    }
}

#endif // !TYPEPARSER_H
