#ifndef SETTINGSMODEL_H
#define SETTINGSMODEL_H

#include <QObject>
#include "settings.h"

class SettingsController;

class SettingsModel : public QObject {
    Q_OBJECT

public:
    SettingsModel(SettingsController* controller);

    ~SettingsModel();

    void initialize();

    Language* getCurrentLanguage();

    const QStringList getLanguages() const;

    const int getCurrentLanguageIndex();

    void setCurrentLanguageIndex(int index);

    Design getDesign();

private:
    SettingsController* controller_;

    Settings* settings_;
};

#endif // !SETTINGSMODEL_H
