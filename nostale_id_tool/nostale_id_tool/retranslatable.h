#ifndef RETRANSLATABLE_H
#define RETRANSLATABLE_H

class Retranslatable {
public:
    virtual void retranslate() = 0;
};

#endif // !RETRANSLATABLE_H
