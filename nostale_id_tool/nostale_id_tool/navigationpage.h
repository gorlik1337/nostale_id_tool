#ifndef NAVIGATIONPAGE_H
#define NAVIGATIONPAGE_H

enum class NavigationPage : char {
    Start = 1,
    ListCreator,
    ItemEditor,
    Settings,
    Credits
};

#endif // !NAVIGATIONPAGE_H
